public aspect ShoppingCartAspect{
        java.util.Date date = new java.util.Date();
	pointcut setBalance(): call(* Wallet.setBalance(int));
	before(int balance): call(* Wallet.setBalance(int)) && args(balance){
          System.out.print("AOP test from Juluru Vinay. This is before Wallet.setBalance(..) is invoked ");
          System.out.println();    
    	}
       after(int balance): call(* Wallet.setBalance(int)) && args(balance){
          System.out.println("policy will be enforced by juluru Vinay in AspectJ");
          System.out.println("the time is now" + date);
          System.out.println("The balance is : " + balance);
}
}
