<?php 
  require("../classes/auth.php");
  require("header.php");
  require("../classes/db.php");
  require("../classes/phpfix.php");
  require("../classes/post.php");
?>

<?php
    if(!isset($_GET["nocsrftoken"]) or ($_GET["nocsrftoken"] !=  $_SESSION["nocsrftoken1"])) {
  	echo "CSRF attack detected";
  	die();
    } else {
    	echo "Deleting the post";
    $post = Post::delete((int)($_GET["id"]));
    header("Location: /admin/index.php");  
     }
?>

