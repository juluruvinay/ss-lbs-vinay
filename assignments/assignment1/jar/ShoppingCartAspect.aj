public aspect ShoppingCartAspect{
java.util.Date date  = new java.util.Date();
pointcut safeWithdrawl(int valueToWithdraw):
call(int Wallet.safeWithdrawl(int)) && args(valueToWithdraw);

    before(int valueToWithdraw):
    safeWithdrawl(valueToWithdraw) {
  try {

     Wallet wallet = new Wallet();

     final int balance = wallet.getBalance();
       if(balance < valueToWithdraw){
        System.out.println("your current balance is not sufficient to make the purchase");
       }
        }catch(Exception e){

            }
   
}
    after(int price) returning (int withdrawAmount): safeWithdrawl(price){
try{
Wallet wallet = new Wallet();

if(withdrawAmount < price){
System.out.println(" the left-out amount in the wallet");
wallet.safeDeposit(withdrawAmount);
   }

}
catch(Exception e){
}
}

} 
